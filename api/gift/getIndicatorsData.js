const pg = require('../postgreSQL/pg_logic');
const x = require('../.runtimeconfig.json');
const firebaseHandler = require('./giftFirebaseHandler');
const cn = x["postgresql-gcp-gift"];


exports.handleGetIndicatorDataRequest = (req, res) => {

    console.log('***Indicators raw download request');
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST');

    if (!this._isValidRequest(req, res)) {
        return;
    }
    console.log(" req valid  " + JSON.stringify( req.body));
    //let reqBody = JSON.parse(req.body);
    let reqBody = req.body;
    let query = this._createQuerySelect(reqBody).then((query) => {
        console.log("Calling .... REVIEW " + query);

        console.log(JSON.stringify(cn));

        pg._query(query, cn).then(
            (data) => {
                res.status(200).send(this._formatResponse(data));
            }
        );

    })


    //res.status(200).send(query)

}

exports._isValidRequest = (req, res) => {
    console.log("***Validating Request");
    //console.log("***>>>Request body is:",req.body);
    let result = true;
    if(req.body == undefined) {

        console.log('---Request body invalid - body undefined');
        res.set('Access-Control-Allow-Origin', "*");
        res.set('Access-Control-Allow-Methods', 'POST');
        res.status(400).send('Body undefined!');
        result = false;
    }

    else if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
        console.log('---Request body invalid');
        res.set('Access-Control-Allow-Origin', "*");
        res.set('Access-Control-Allow-Methods', 'POST');
        res.status(400).send('No body defined!');
        result = false;
    }
    return result;
}

exports._createPgQuery = (reqBody) => {
    let res = '';
    let select = this._createQuerySelect(reqBody);
    //let where = this._createQueryWhere(reqBody);
    res = select + /*where + */';';
    return res;
};

exports._createQuerySelect = (reqBody) => {

    return new Promise((resolve, reject) => {
        let res = '';
        let s_code = reqBody.survey_code
        let code = '';
        if (reqBody.survey_code != null)
            code = s_code.split("_");
        let group = reqBody.survey_code;
        console.log('Indicator ' + reqBody.indicator)



        /* FILTERS */

        /* age filters   :: using casting for int*/
        let place_holder_age_filter = '';
        place_holder_age_filter = 'and age_year::int >=' + reqBody.parameters.age_year.from + ' and age_year::int <= ' + reqBody.parameters.age_year.to;


        /*-------------- gender --------------------*/

        console.log('gender : ' + reqBody.parameters.gender)
        let place_holder_gender_filter = ''
        if (reqBody.parameters.gender != null)
            place_holder_gender_filter = 'and sex = ' + reqBody.parameters.gender;

        /*---------- Nutrient -----------------------*/

        let place_holder_nutrient = ''
        if (reqBody.codes != null)
            place_holder_nutrient = 'fct_dwh_consumption.' + reqBody.codes


        /*--------- SPECIAL CONDITION  ----------------*/

        let place_holder_special_filter = ''
        if (reqBody.parameters.special_condition != null && reqBody.parameters.special_condition.length > 0) {
            place_holder_special_filter = 'and special_condition in (' + reqBody.parameters.special_condition + ')'
            console.log('LENGTH : ' + reqBody.parameters.special_condition.length);
        }




        switch (reqBody.indicator) {
            case 1:
                console.log(' case 1')
                firebaseHandler._retrieveQuery('query_1').then((Q) => {
                    let query_to_call = Q.query;

                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);
                    console.log("This is it 01 :  " + query_to_call);
                    resolve(query_to_call)

                })
                break;
            case 2:
                console.log(' case 2')
                firebaseHandler._retrieveQuery('query_2').then((Q) => {
                    let query_to_call = Q.query;

                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);

                    console.log("This is it 02 :  " + query_to_call);
                    resolve(query_to_call)
                })

                break;

            case 3:
                console.log(' case 3')
                firebaseHandler._retrieveQuery('query_3_level_' + reqBody.level).then((Q) => {
                    let query_to_call = Q.query;

                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);

                    if (reqBody.level === 2 && reqBody.codes != null) {
                        let subgroup = parseInt(reqBody.codes);
                        while (query_to_call.includes('place_holder_sub_code'))
                            query_to_call = query_to_call.replace('place_holder_sub_code', 'and fct_dwh_consumption.group_code::text =' + '\'' + subgroup + '\'');
                    }

                    console.log("This is it 03 :  " + query_to_call);
                    resolve(query_to_call)
                })

                break;
            case 0:
                console.log(' case 0')
                resolve('SELECT * FROM gift.vw_gift_indicator_' + this._paddy(reqBody.indicator, 2, 0) + ' v')
                break;
            case 6:
                console.log(' case 6')
                // resolve( 'SELECT * FROM gift.vw_gift_indicator_' + this._paddy(reqBody.indicator,2,0) +'_'+reqBody.codes+ ' v WHERE survey_code = \''+code[2]+'\'');        

                firebaseHandler._retrieveQuery('query_6_level_' + reqBody.level).then((Q) => {
                    let query_to_call = Q.query;
                    let group_code = parseInt(reqBody.group)
                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_group_code'))
                        query_to_call = query_to_call.replace('place_holder_group_code', group_code);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_nutrient'))
                        query_to_call = query_to_call.replace('place_holder_nutrient', place_holder_nutrient);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);


                    console.log("This is it 06 :  " + query_to_call);
                    resolve(query_to_call)
                })
                break;
            case 4:
                console.log(' case 4')
                //resolve( 'SELECT * FROM gift.vw_gift_indicator_' + this._paddy(reqBody.indicator,2,0) +'_'+this._paddy(reqBody.group,2,0)+ ' v WHERE survey_code = \''+code[2]+'\'');        

                console.log(' reqBody.level : ' + reqBody.level);

                firebaseHandler._retrieveQuery('query_4_level_' + reqBody.level).then((Q) => {
                    let query_to_call = Q.query;
                    let group_code = parseInt(reqBody.group)
                    let subgroup_code = parseInt(reqBody.subgroup)
                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_group_code'))
                        query_to_call = query_to_call.replace('place_holder_group_code', group_code);
                    while (query_to_call.includes('place_holder_sub_code'))
                        query_to_call = query_to_call.replace('place_holder_sub_code', subgroup_code);
                    while (query_to_call.includes('place_holder_food_code'))
                        query_to_call = query_to_call.replace('place_holder_food_code', reqBody.food);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);

                    console.log("This is it 04 :  " + query_to_call);
                    resolve(query_to_call)
                })

                break;

            case 7:
                console.log(' case 7')
                //resolve( 'SELECT * FROM gift.vw_gift_indicator_' + this._paddy(reqBody.indicator,2,0) +'_'+this._paddy(reqBody.group,2,0)+ ' v WHERE survey_code = \''+code[2]+'\'');        

                firebaseHandler._retrieveQuery('query_7_level_' + reqBody.level).then((Q) => {
                    let query_to_call = Q.query;
                    let group_code = parseInt(reqBody.group)
                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_group_code'))
                        query_to_call = query_to_call.replace('place_holder_group_code', group_code);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);


                    console.log("This is it 07 :  " + query_to_call);
                    resolve(query_to_call)
                })

                break;

            case 8:
                console.log(' case 8')
                firebaseHandler._retrieveQuery('query_8').then((Q) => {
                    let query_to_call = Q.query;

                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);

                    console.log("This is it 08 :  " + query_to_call);
                    resolve(query_to_call)
                })

                break;


                case 9:
                console.log(' case 9')
                firebaseHandler._retrieveQuery('query_9').then((Q) => {
                    let query_to_call = Q.query;

                    while (query_to_call.includes('place_holder_survey_code'))
                        query_to_call = query_to_call.replace('place_holder_survey_code', code[2]);
                    while (query_to_call.includes('place_holder_age_filter'))
                        query_to_call = query_to_call.replace('place_holder_age_filter', place_holder_age_filter);
                    while (query_to_call.includes('place_holder_gender_filter'))
                        query_to_call = query_to_call.replace('place_holder_gender_filter', place_holder_gender_filter);
                    while (query_to_call.includes('place_holder_special'))
                        query_to_call = query_to_call.replace('place_holder_special', place_holder_special_filter);

                    console.log("This is it 09 :  " + query_to_call);
                    resolve(query_to_call)
                })

                break;    
            default:
                console.log(' case default')
                resolve('SELECT * FROM gift.vw_gift_indicator_' + this._paddy(reqBody.indicator, 2, 0) + ' v WHERE survey_code = \'' + code[2] + '\'');
        }
    })

};

exports._paddy = (num, padlen, padchar) => {
    var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
    var pad = new Array(1 + padlen).join(pad_char);
    return (pad + num).slice(-pad.length);
}

// exports._createQueryWhere = (reqBody) => {
//     return "";
// };

exports._formatResponse = (data) => {
   
    let response = {};
    for (var i = 0; i < data.length; i++) {
        let currentItem = data[i];
        response[currentItem.level] = [];
        for (var j = 0; j < currentItem.json_agg.length; j++) {
            var keys = Object.keys(currentItem.json_agg[j]);
            let myArr = [];
            keys.forEach(function (key) { myArr.push(currentItem.json_agg[j][key]); });
            response[currentItem.level].push(myArr);
        }
    }
    
    return response;
}

exports.handleGetFilteredMetadata = (req, res) => {

    console.log('***Indicators filters request');
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST');

    if (!this._isValidRequest(req, res)) {
        return;
    }
    let reqBody = JSON.parse(req.body);

    firebaseHandler._retrieveQuery('filter_metadata_ui').then((Q) => {
        let query_to_call = Q.query;
        let food_array = '';
        let special_condition = '';
        let l = 0;

        // foodex2_code filter
        if (reqBody.foodex2_code !== 'undefined' && reqBody.foodex2_code != null) {
            l = reqBody.foodex2_code.codes[0].codes.length;

            for (var i = 0; i < l - 1; i++) {
                food_array = food_array + '\'' + reqBody.foodex2_code.codes[0].codes[i] + '\',';
            }
            food_array = food_array + '\'' + reqBody.foodex2_code.codes[0].codes[i] + '\'';

            while (query_to_call.includes('food_codes_place_holder'))
                query_to_call = query_to_call.replace('food_codes_place_holder',
                    'and foodex2_code in (' + food_array + ')');
        }
        else query_to_call = query_to_call.replace('food_codes_place_holder',
            ' ');


        // gender filter    
        if (reqBody.gender !== 'undefined' && reqBody.gender != null)
            query_to_call = query_to_call.replace('sex_place_holder',
                'and sex = ' + reqBody.gender.codes[0].codes[0]);
        else query_to_call = query_to_call.replace('sex_place_holder',
            ' ');


        // special condition filter    
        if (reqBody.special_condition !== 'undefined' && reqBody.special_condition != null) {
            l = reqBody.special_condition.codes[0].codes.length;
            for (var i = 0; i < l - 1; i++) {
                special_condition = special_condition + '\'' + reqBody.special_condition.codes[0].codes[i] + '\',';
            }
            special_condition = special_condition + '\'' + reqBody.special_condition.codes[0].codes[i] + '\'';
            query_to_call = query_to_call.replace('special_condition_holder',
                'and special_condition in (' + special_condition + ')');
        }
        else query_to_call = query_to_call.replace('special_condition_holder',
            ' ');

        // age year filter  
        if (reqBody.age_year !== 'undefined' && reqBody.age_year != null) {
            let from = reqBody.age_year.number[0].from;
            let to = reqBody.age_year.number[0].to;
            query_to_call = query_to_call.replace('age_year_holder',
                'and age_year::int >=' + from + ' and age_year::int < +' + to);
        }
        else query_to_call = query_to_call.replace('age_year_holder',
            ' ');


        // age month filter  
        if (reqBody.age_month !== 'undefined' && reqBody.age_month != null) {
            let from = reqBody.age_month.number[0].from;
            let to = reqBody.age_month.number[0].to;
            query_to_call = query_to_call.replace('age_month_holder',
                'and age_month >=' + from + ' and age_month < +' + to);
        }
        else query_to_call = query_to_call.replace('age_month_holder',
            ' ');

        console.log('query to call ' + query_to_call);

        pg._query(query_to_call, cn).then(
            (data) => {
                if (data[0].json_agg == null)
                    res.status(200).send({});
                else
                    res.status(200).send(data);
            }
        );
    });

}




