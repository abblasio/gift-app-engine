const sha = require('sha256');
const request = require('request-promise');
const pg = require('../postgreSQL/pg_logic');
const storage = require('../cloudHelpers/storageHelper');

const x = require('../.runtimeconfig.json');
const jenkinsAuth = x["jenkins-cloud-config"];
const cn = x["postgresql-gcp-gift"];

exports.handleUploadDataRequest = (req,res) => {
   
    //Enable CORS in response
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST');    

    if (!this._isValidRequest(req,res)){
        res.status(400).send("ERROR ===> Invalid request body format.\nExample:\n" +
                         "{\"action\":\"[upload|get_execution_info]\",\n\"parameters\":{\n\"countryIso3\":\"BGD\",\n\"surveyCode\":\"000023BGD201001\",\n" + 
                         "\"bucketName\":\"MyCGPBucket\",\n\"fileName\":\"MyUploadedZIPFile.zip\",\n}\n}");
        return;
    }
    
    //get parameters
    let body = JSON.parse(req.body);
    let action = body.action;
    
    switch(action){
        case "upload_data":
            this.handlePutRequest(req).then(
                (hash)=>{
                    res.status(200).send(hash);
                },
                (err)=>{
                    res.status(500).send(err);
                }
            );
        break;
        case "get_execution_info":
            this.handleGetExecutionInfoRequest(req).then(
                (data)=>{
                    res.status(200).send(data);
                },
                (err)=>{
                    res.status(500).send(err);
                }
            );
        break;
    }    
}

exports.handleGetDataRequest = (req) =>{

    //Enable CORS in response
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST');    

    if (!this._isValidGetDataRequest(req)){
        res.status(400).send("ERROR ===> Invalid request body format.\nExample:\n" +
                            "{\"parameters\":{\n\"countryIso3\":\"BGD\",\n\"surveyCode\":\"000023BGD201001\",\n}\n}");
        return;
    }
    

    let body = JSON.parse(req.body);
    let parameters = body.parameters;
    let pCatalog = parameters.catalog ;
    let pCountry = parameters.countryIso3;
    let pSurvey = parameters.surveyCode;

    if(pCatalog===undefined || !pCatalog.trim()){
        reject("Invalid Catalog.");
    }else{
        let query = "";
        if(pCatalog=='subject')
            query = "SELECT * FROM public.fct_ods_subject where country_iso3 = '" + pCountry + "' and survey_code = '" + pSurvey + "';";
        else if(pCatalog=='consumption')
            query = "SELECT * FROM public.fct_ods_consumption where country_iso3 = '" + pCountry + "' and survey_code = '" + pSurvey + "'";

        pg._query(query ,cn)
            .then((data)=> {
                res.status(200).send(data);
            }).catch((error)=> {
                res.status(500).send(error);
        });
    }
}

exports.handleGetExecutionInfoRequest = (req) =>{
    return new Promise((resolve,reject)=>{

        let body = JSON.parse(req.body);
        let parameters = body.parameters;
        let pExecutionId = parameters.executionId ;

        if(pExecutionId===undefined || !pExecutionId.trim()){
            reject("Invalid Execution ID.");
        }else{
            
            let query = "SELECT execution_time, execution_key, param_country, param_survey, param_bucket, param_file,    type,  message, message_code ";
            query = query + "FROM public.gift_audit_etl x ";
            query = query + "WHERE execution_key = '" + pExecutionId + "' ";
            query = query + "AND execution_time = (select max(execution_time) from public.gift_audit_etl where execution_key = x.execution_key);";
            
            console.log("Get info - connection case query " + query);
            pg._query(query ,cn)
                .then((data)=> {
                    resolve(data[0]);
                }).catch((error)=> {
                    reject(error);
            });
        }
    });
}

exports.handlePutRequest = (req) =>{
    return new Promise((resolve,reject)=>{
        let body = JSON.parse(req.body);
        let parameters = body.parameters;
        let pCountry = parameters.countryIso3;
        let pSurvey = parameters.surveyCode;
        let env = 'PROD_GCP';
        //let pBucket = parameters.bucketName;
        let pFile = parameters.fileName;
        let bucketName = 'fao-gift.appspot.com';
        let bucketName_public = 'fao-gift-bucket-public';

        //Generate hash for execution key
        //let charSet = pCountry+pSurvey+pBucket+pFile + new Date();
        let charSet = pCountry+pSurvey+pFile + new Date();
        let hashKey = sha(JSON.stringify(charSet).trim());
        //Prepare URL
        let url = "http://35.204.132.173/jenkins/job/TalendJobs/job/gift_upload_data/buildWithParameters?token=TOKENDELAMUERTE"
                + "&bucket_name=" + bucketName
                + "&bucket_name_public=" + bucketName_public 
                + "&file_name=" + pFile 
                + "&country=" + pCountry
                + "&survey=" + pSurvey
                + "&env=" + env
                + "&execution_key=" + hashKey;
        //Build method
        let method = {
            method: 'GET',
            uri: url,
            headers: {
                Authorization: 'Basic ' + new Buffer(jenkinsAuth.auth.user + ':' + jenkinsAuth.auth.pass).toString('base64')
            }
        };
        //Call run package
        request(method)
            .then(rBody => {
                let jRes = {};
                jRes['executionId'] = hashKey;
                resolve(jRes);
            })
            .catch(err => {
                console.error("error: " + err);
                //let statusCode = (err.statusCode) ? err.statusCode : 500;
                //let errBody = (err.message) ? err.message : err;
                //res.status(statusCode).send(errBody);
                reject(err);
                // POST failed...
        });
    });
}

exports._isValidRequest = (req,res) => {
    console.log("***Validating Request");
    let result = true;
    //Check for existance of body
    if (req.body.constructor === Object && Object.keys(req.body).length === 0 ) {
        result = false;
    }else {
        let body = JSON.parse(req.body);
        //Check for existance of action parameter
        if(body.action === undefined || !body.action.trim()){
            console.log("The parameter countryIso3 is not defined");
            let result = false; 
        }else{
            let action = body.action;

            //Check for existance of parameters list parameter
            if (body.parameters.constructor === Object && Object.keys(body.parameters).length === 0 ) {
                res.status(400).send('No parameters defined in body!');
                result = false;
            }else{
                let parameters = body.parameters;

                if( action == 'upload_data'){
                    let pCountry = parameters.countryIso3;
                    let pSurvey = parameters.surveyCode;
                    //let pBucket = parameters.bucketName;
                    let pFile = parameters.fileName;
                    if(pCountry === undefined || !pCountry.trim()){
                        console.log("The parameter countryIso3 is not defined");
                        let result = false; 
                    }else if(pSurvey === undefined || !pSurvey.trim()){
                        console.log("The parameter surveyCode is not defined");
                        let result = false; 
                    /*}else if(pBucket === undefined || !pBucket.trim()){
                        console.log("The parameter bucketName is not defined");
                        let result = false; */
                    }else if(pFile === undefined || !pFile.trim()){
                        console.log("The parameter fileName is not defined");
                        let result = false; 
                    }else{
                        let result = true;
                    }

                }else if(action == 'get_execution_info'){
                    console.log("get_execution_info case");
                    let pExecutionId = parameters.executionId;
                    if(pExecutionId === undefined || !pExecutionId.trim()){
                        console.log("The parameter execution id is not defined");
                        let result = false; 
                    }
                }
            }
        }
    }
    return result;
}

exports._isValidGetDataRequest = (req,res) => {
    console.log("***Validating Get Data Request");
    let result = true;
    //Check for existance of body
    if (req.body.constructor === Object && Object.keys(req.body).length === 0 ) {
        result = false;
    }else {
        let body = JSON.parse(req.body);

        //Check for existance of parameters list parameter
        if (body.parameters.constructor === Object && Object.keys(body.parameters).length === 0 ) {
            res.status(400).send('No parameters defined in body!');
            result = false;
        }else{
            let parameters = body.parameters;

            let pCountry = parameters.countryIso3;
            let pSurvey = parameters.surveyCode;
            if(pCountry === undefined || !pCountry.trim()){
                console.log("The parameter countryIso3 is not defined");
                let result = false; 
            }else if(pSurvey === undefined || !pSurvey.trim()){
                console.log("The parameter surveyCode is not defined");
                let result = false; 
            }

        }
    }
    return result;
}

exports.handleListFilesrequest = (req,res) =>{
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST');    
    let bucketName = 'fao-gift-bucket-public';

    let jsonRes = [];
    storage._getFileListFromBucket(bucketName).then((files)=> {
        for(var i =0;i<files.length;i++){
            let folder = files[i].name.split("/")[0];
            let subFolder = files[i].name.split("/")[1];
            let file = files[i].name.split("/")[2];
            let pLink = `https://storage.googleapis.com/${bucketName}/${folder}/${subFolder}/${file}`;
            console.log('pLink : ' + pLink);
            jsonRes.push({country:  folder,
                          survey:  subFolder,
                          file: file.replace(folder,"").replace(subFolder,"").replace(".csv","").replace(/_/g,""),
                          url:pLink});
        }
        res.status(200).send(jsonRes);
    }).catch((error)=> {
        res.status(500).send(error);
    });
}




exports.handleDownloadrequest = (req,res) =>{
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'POST'); 
    
    let reqBody = JSON.parse(req.body);
    
    console.log("===>Looking for Metadata : ",reqBody.reference);
    if (typeof reqBody.reference == "undefined"){
        res.status(400).send("ERROR - Missing reference parameter" + JSON.stringify(reqBody));
        return;
    }


    let bucketName = 'fao-gift-bucket-public';
    let index = 0;
    let jsonRes = [];
    let folder = '';
    let subFolder = '';
    let file = '';
    let zipExtension = '.zip';
    storage._getFileListFromBucket(bucketName).then((files)=> {
        for(var i =0;i<files.length;i++){
            
            let info = files[i].name.split("/");
            if (info[1] === reqBody.reference ){

                 folder = info[0];
                 subFolder = info[1];
                 file = info[2];                
           
            if(file.endsWith(zipExtension)){
            console.log('Found ZIP');
            let pLink = `https://storage.googleapis.com/${bucketName}/${folder}/${subFolder}/${file}`;

            jsonRes.push({country:  folder,
                          survey:  subFolder,
                          file: file.replace(folder,"").replace(subFolder,"").replace(".csv","").replace(/_/g,""),
                          url:pLink});
  
        }
    }
    }
        res.status(200).send(jsonRes);
    }).catch((error)=> {
        res.status(500).send(error);
    });
}