const admin = require('firebase-admin');

var serviceAccount = require('../credentials/fao-gift-firebase-adminsdk-ujpbu-4fea6084df.json');
const settings = {timestampsInSnapshots: true};

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
});

var db = admin.firestore();
db.settings(settings);

exports.giftGetFirebaseData = (req, res) => {
    res.set("Access-Control-Allow-Credentials", "true");
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET');
    /*res.set("Access-Control-Allow-Headers", "content-type, authorization,if-none-match");
    res.set("Access-Control-Max-Age", "3600");
    if(req.method=='OPTIONS'){
        res.set(200).send();
        return;
    }*/
    //this.validateToken(req).then((email)=>{
    /*res.status(200).send(req.query);
    return;*/
    let confidentialityStatusfive = false;
    if (req.body != "undefined" && req.method === "POST") {
         //let reqBody = JSON.parse(req.body);
         let reqBody = req.body;
        //console.log( "POSTO " + JSON.stringify(req.body));
        confidentialityStatusfive = reqBody.confidentiality === '5';
        //console.log("gimme five " + confidentialityStatusfive);
    }
    console.log("===>Query is this : ", req.query);
    if (typeof req.query.catalog == "undefined") {
        res.status(400).send("ERROR - Missing catalog parameter");
        return;
    }

    const params = {
        catalog: req.query.catalog,
        document: undefined
    }

    if (typeof req.query.document != "undefined") {
        params.document = req.query.document;
    }
    console.log("===>Parameters are: ", params);
    var whiteListNode = db.collection('gcWhiteListNodes');
    whiteListNode.get()
        .then(catalogs => {
            let allow = false;
            catalogs.forEach(node => {
                if (params.catalog == node.data().node) {
                    allow = true;
                }
            });

            if (allow) {
                let jobList = [];
                let jobRefs;

                if (typeof params.document == "undefined") {
                   
                    jobRefs = db.collection(params.catalog);
                    jobRefs.get()
                        .then(jobs => {
                            jobs.forEach(job => {
                               if (confidentialityStatusfive) {
                                    if (job.data().meAccessibility.seConfidentiality.confidentialityStatus.codes[0].code == '5')
                                        jobList.push(job.data());
                                } 
                                 else                                  
                                    jobList.push(job.data());
                            });
                            res.status(200).send(jobList);
                        })
                        .catch(err => {
                            console.log('Error getting documents for node: ', err);
                            res.status(500).send(jobList);
                        });
                } else {

                    jobRefs = db.collection(params.catalog).doc(params.document);
                    jobRefs.get()
                        .then(doc => {
                            if (doc.exists) {
                                res.status(200).send(doc.data());
                            } else {
                                res.status(400).send(`Document ${params.document} in catalog ${params.catalog} doesnt exists.`);
                            }
                        })
                        .catch(err => {
                            console.log('Error getting documents for catalog ', params.catalog, ' and document ', params.document, ': ', err);
                            res.status(500).send(jobList);
                        });
                }
            } else {
                res.status(403).send(`Acesss to catalog ${params.catalog} is not allowed.`);
                return;
            }

        })
        .catch(err => {
            console.log('Error getting whitelisted nodes: ', err);
            res.status(500).send(err);
        });
}

exports.giftGetAcknowledgmentData = (req, res) => {
    res.set("Access-Control-Allow-Credentials", "true");
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET');

    console.log("===>Query is: ", req.query);
    if (typeof req.query.survey == "undefined") {
        res.status(400).send("ERROR - Missing survey code parameter");
        return;
    }

    if (typeof req.query.country == "undefined") {
        res.status(400).send("ERROR - Missing country code parameter");
        return;
    }

    const params = {
        surveyCode: req.query.survey,
        countryCode: req.query.country
    }

    var query = db.collection('giftmetadataresource');
    query.where('country', '==', params.countryCode)
        .where('reference', '==', params.surveyCode)
        .get()
        .then(data => {

            let rDisc = '';
            if (data.size > 1) {
                res.status(400).send("Wrong results, please verify parameters.");
                return;
            }
            if (data.size == 0) {
                rDisc = `FAO-WHO Gift data acknowledgment\n${params.countryCode}-${params.surveyCode}\n\nThere is now acknowledgment available for this data yet.\n`;
                res.status(200).send(rDisc);
                return;
            }
            if (data.size == 1) {
                data.forEach(row => {
                    let disclaimerValue = '';
                    if (row.data().disclaimerValue == undefined) {
                        disclaimerValue = 'There is now acknowledgment available for this data yet.';
                    } else {
                        disclaimerValue = row.data().disclaimerValue.replace(/<[^>]*>/g, '');
                    }
                    rDisc = `FAO-WHO Gift data acknowledgment\n${params.countryCode}-${params.surveyCode}\n\n${disclaimerValue}\n`;
                });
                res.status(200).send(rDisc);
                return;
            }
        })
        .catch(err => {
            console.log('Error getting documents', err);
            res.status(500).send("Error retrieving data from Cloud Firestore." + err);
        });
}



exports.giftGetMetadataResource = (req, res) => {

    res.set("Access-Control-Allow-Credentials", "true");
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET');

    //let reqBody = JSON.parse(req.body);
    let reqBody = req.body;
    // let confidentialityStatusfive = reqBody.meAccessibility.seConfidentiality.confidentialityStatus.codes[0].codes[0] === '5';

    console.log("===>Looking for Metadata : ", reqBody.reference);
    if (typeof reqBody.reference == "undefined") {
        res.status(400).send("ERROR - Missing reference parameter" + JSON.stringify(reqBody));
        return;
    }


    var query = db.collection('giftmetadataresource');
    query.where('reference', '==', reqBody.reference)
        .get()
        .then(data => {

            let metadata = '';
            if (data.size > 1) {
                res.status(400).send("Wrong results, please verify parameters.");
                return;
            }
            if (data.size == 0) {
                rDisc = `FAO-WHO Gift metadata \n${reqBody.reference}\n\nThere is no metadata available for this data yet.\n`;
                res.status(200).send(rDisc);
                return;
            }
            if (data.size == 1) {
                data.forEach(row => {
                    metadata = row.data();
                });
                res.status(200).send(metadata);
                return;
            }
        })
        .catch(err => {
            console.log('Error getting documents', err);
            res.status(500).send("Error retrieving data from Cloud Firestore." + err);
        });
}



exports.giftGetDisclaimer = (req, res) => {
    console.log("Starting giftGetDisclaimer");
    res.set("Access-Control-Allow-Credentials", "true");
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET');

    let reqBody = JSON.parse(req.body);

    console.log("===>Looking for disclaimer of Survey Code : ", reqBody.reference);
    if (typeof reqBody.reference == "undefined") {
        res.status(400).send("ERROR - Missing reference parameter" + JSON.stringify(reqBody));
        return;
    }


    var query = db.collection('giftmetadataresource');
    query.where('reference', '==', reqBody.reference)
        .get()
        .then(data => {

            let metadata = '';
            if (data.size > 1) {
                res.status(400).send("Wrong results, please verify parameters.");
                return;
            }
            if (data.size == 0) {
                rDisc = `FAO-WHO Gift metadata \n${reqBody.reference}\n\nThere is no metadata available for this data yet.\n`;
                res.status(200).send(rDisc);
                return;
            }
            if (data.size == 1) {
                data.forEach(row => {
                    metadata = row.data();
                });
                res.status(200).send(JSON.stringify(metadata.disclaimerValue));
                return;
            }
        })
        .catch(err => {
            console.log('Error getting documents', err);
            res.status(500).send("Error retrieving data from Cloud Firestore." + err);
        });
}



exports._retrieveQuery = (query) => {
    return new Promise((resolve, reject) => {
        console.log("looking for .... " + query)
        var pQuery = db.collection('postgres_query').doc(query);
        pQuery.get().then(doc => {
            if (doc.exists) {
                console.log("Found " + doc.data());
                resolve(doc.data());
            }
            else {
                reject("Query not found");
            }
        })
    })
}

exports._cloneMdsd = (catalog, document) => {
    return new Promise((resolve, reject) => {
        console.log("looking for mdsd string .... " + catalog + ' document ' + document);
        var pQuery = db.collection(catalog).doc(document);
        console.log("pQuery ready .... ");
        pQuery.get().then(doku => {
            if (doku.exists) {
                console.log("Found mdsd....now cloning");

                resolve(db.collection('mdsd1').doc(document).set(doku.data()));
            }
            else {
                reject("mdsd not found");
            }
        })
    })
}