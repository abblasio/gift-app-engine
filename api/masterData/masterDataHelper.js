const pg = require('../postgreSQL/pg_logic');
const x = require('../.runtimeconfig.json');
const cn = x["postgresql-gcp-wiewsdb"];

/* This is a test*/


const catalogs ={
    /*country:{
        idField:"id",
        nameFieldPrefix:["name_"],
        table:"dim.dim_country",
        order:{
            field:"id",
            type:"asc"
        }
    },*/
    nocs:{
        idField:"iso3",
        nameFieldPrefix:["iso3","label_", "list_name_", "full_name_", "adjective_", "capital_city_", "curr_unit_", "curr_symbol_", "sort_order_"],
        table:"dim.dim_nocs",
        order:{
            field:"iso3",
            type:"asc"
        }
    },
    gaul1:{
        idField:"iso3",
        nameFieldPrefix:["iso3","gaul_level_0_code","gaul_level_0_name","ogc_fid","gaul_level_1_code","gaul_level_1_name","str1_year","exp1_year","status","disp_area","shape_leng","shape_area"],
        table:"dim.vgaul_level_1",
        order:{
            field:"iso3,gaul_level_0_name",
            type:"asc"
        }
    },
    gaul2:{
        idField:"gaul_level_2_code",
        nameFieldPrefix:["gaul_level_1_code","gaul_level_1_name","ogc_fid","gaul_level_2_code","gaul_level_2_name","str2_year","exp2_year","status","disp_area","adm0_code","adm0_name","shape_leng","shape_area"],
        table:"dim.vgaul_level_2",
        order:{
            field:"gaul_level_2_name",
            type:"asc"
        }
    }
}

const allowedLangs = {
    //country:['en'],
    nocs:['ar','en','es','fr','it','ru','zh'],
    gaul1:['en'],
    gaul2:['en']
};



exports.handleRequest = (req,res) => {
    if (!this._isValidRequest(req, res))
        return;
    this._sendRequest(req,res);
}

exports._sendRequest = (req,res) => {
    try{
        //Enable Cors
        res.set('Access-Control-Allow-Origin', "*");
        res.set('Access-Control-Allow-Methods', 'POST');

        console.log('***Processing request.');
        //Define variables
        let catalog = req.query.catalog;
        let lang = req.query.lang;
        let filterField;
        let filterValue;
        let sortField;
        let sortOrder;
        

        
        //Validate filterField
        if( typeof req.query.filterField !== "undefined" && req.query.filterField !== null && !req.query.filterField !== ''){
            if(catalog.toLowerCase()=='nocs' && req.query.filterField !== 'iso3'){
                if(catalogs[catalog].nameFieldPrefix.indexOf(req.query.filterField.replace(lang,'')) == -1){
                    res.status(400).send(`Parameter filterField with value "${req.query.filterField}" does not exist in catalog field list, check parameters.`);
                    return;
                }else{
                    filterField = req.query.filterField;
                }
            }else{
                if(catalogs[catalog].nameFieldPrefix.indexOf(req.query.filterField) == -1){
                    res.status(400).send(`Parameter filterField with value "${req.query.filterField}" does not exist in catalog field list, check parameters.`);
                    return;
                }else{
                    filterField = req.query.filterField;
                }
            }
        }else{
            filterField=undefined;
        }

        //Validate sortField
        if( typeof req.query.sortField !== "undefined" && req.query.sortField !== null && !req.query.sortField !== ''){
            
            if(catalog.toLowerCase()=='nocs' && req.query.sortOrder !== 'iso3'){
                if(catalogs[catalog].nameFieldPrefix.indexOf(req.query.sortField.replace(lang,'')) == -1){
                    res.status(400).send(`Parameter sortField with value "${req.query.sortField}" does not exist in catalog field list, check parameters.`);
                    return;
                }else{
                    sortField = req.query.sortField;
                }
            }else{
                if(catalogs[catalog].nameFieldPrefix.indexOf(req.query.sortField) == -1){
                    res.status(400).send(`Parameter sortField with value "${req.query.sortField}" does not exist in catalog field list, check parameters.`);
                    return;
                }else{
                    sortField = req.query.sortField;
                }
            }
            
        }else{
            sortField=undefined;
        }

        //res.status(200).send(req.query);

        //Validate filterValue
        if( typeof req.query.filterValue !== "undefined" && req.query.filterValue !== null && !req.query.filterValue !== ''){
            filterValue = req.query.filterValue;
        }else{
            filterValue=undefined;
        }
        //Validate sort order
        if( typeof req.query.sortOrder !== "undefined" && req.query.sortOrder !== null && !req.query.sortOrder !== ''){
            if(req.query.sortOrder.toLowerCase() == 'asc' || req.query.sortOrder.toLowerCase() == 'desc'){
                sortOrder = req.query.sortOrder;
            }else{
                res.status(400).send(`Parameter sortOrder with value "${req.query.sortOrder}" is not supported, values allowed are ASC or DESC.`);
                return;
            }
        }else{
            sortOrder=undefined;
        }
        //res.status(200).send(req.query);

        //Validate that both parameters are not being used
        /*if (typeof filterValue != "undefined" && typeof parentId != "undefined"){
            res.status(400).send("Parameter id and parameter parentId cannot be combined, check parameters.");
            return;
        }*/

        console.log("***Catalog: ",catalog,"- Lang: ",lang);

        //Query for getting configuration data
        let qData = this._createPgQuery(catalog,lang,filterField,filterValue,sortField,sortOrder);

        //res.status(200).send(qData);
        //return;

        pg._query(qData ,cn)
        .then( (data)=> {
                res.status(200).send(data);
        }
        ).catch((error)=> {
            res.set('Access-Control-Allow-Origin', "*");
            res.set('Access-Control-Allow-Methods', 'POST'); 
            res.status(500).send(error);
        });

    }catch(err){
        res.set('Access-Control-Allow-Origin', "*");
        res.set('Access-Control-Allow-Methods', 'POST'); 
        res.status(500).send(err);
    }
}

exports._isValidRequest = (req, res) => {
    console.log("***Validating Request");
    //console.log("***>>>Request body is:",req.body);
    let result = true;
    if (req.body === undefined) {
        res.status(400).send('No body defined!');
        result = false;
    }
    else if (req.query === undefined || req.query.catalog === undefined || req.query.catalog === null || req.query.catalog === '')
    {
        res.status(400).send('No catalog defined in parameters!');
        result = false;
    }
    else if(req.query === undefined || req.query.lang === undefined || req.query.lang === null || req.query.lang === ''){
        res.status(400).send('No lang defined in parameters!');
        result = false;
    }
    else if (catalogs[req.query.catalog.toLowerCase()] === undefined || catalogs[req.query.catalog.toLowerCase()] === null || catalogs[req.query.catalog.toLowerCase()] === '')
    {
        res.status(400).send('Catalog doesnt exist!');
        result = false;
    }
    else if(allowedLangs[req.query.catalog.toLowerCase()].indexOf(req.query.lang.toLowerCase()) < 0){
        res.status(400).send('Lang not allowed for this catalog!');
        result = false;
    }

    return result;
}

exports._createPgQuery = (catalog,lang,filterField,filterValue,sortField,sortOrder) => {
    console.log("***===>Creating query");
    try{
        let resQuery = "";
        //var resQuery = "SELECT " + catalogs[catalog].idField + ',' + catalogs[catalog].nameFieldPrefix + lang.toLowerCase() + ' FROM ' + catalogs[catalog].table + ' ORDER BY 1 ASC;';
        //let qId = "SELECT " + catalogs[catalog].idField + ', ';
        let qId = "SELECT ";
        let qLabels = "";
        var aLabels = [];
        
        if (catalog=='nocs'){
            for(i=0;i< catalogs[catalog].nameFieldPrefix.length;i ++ ){
                if(catalogs[catalog].nameFieldPrefix[i] == catalogs[catalog].idField){
                    aLabels.push(catalogs[catalog].nameFieldPrefix[i]); 
                }else{
                    aLabels.push(catalogs[catalog].nameFieldPrefix[i] + lang.toLowerCase()); 
                }
                
            }
        }else{
            aLabels = catalogs[catalog].nameFieldPrefix;
        }
        qLabels = aLabels.join(' , ');


        let qFrom = " FROM " + catalogs[catalog].table + ' ';
        
        let qWhere ="";
        
        if(typeof filterField == "undefined"){
            qWhere = "WHERE " + catalogs[catalog].idField + ' is not null';
        }else{
            qWhere = "WHERE " + filterField + "::text= '"+ filterValue + "'";
        }
        
        let order = '';
        if(typeof sortField == "undefined"){
            order = " ORDER BY " + catalogs[catalog].order.field + ' ' + catalogs[catalog].order.type + ';';
        }else{
            if(typeof sortOrder == "undefined"){
                order = " ORDER BY " + sortField + ' ASC;';
            }else{
                order = " ORDER BY " + sortField + ' ' + sortOrder + ';';
            }
        }

        resQuery = qId + qLabels + qFrom + qWhere + order;
        return resQuery;
    }catch(error){
        res.set('Access-Control-Allow-Origin', "*");
        res.set('Access-Control-Allow-Methods', 'POST'); 
        res.status(500).send(error);
    }   
};

