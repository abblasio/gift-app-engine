const pgp = require('pg-promise')();

exports._query = (query,connection) => {
    return new Promise((resolve, reject) => {
        const db = pgp(connection);
        db.any(query)
            .then(
                data => {
                    resolve(data);
                }
            ).catch(
                error => {
                    reject(error);
                }
            );
    });
}


exports._batch = (queryArray,connection) => {
    console.log('***Running batch queries');
    return new Promise((resolve, reject) => {
        try{
            const db = pgp(connection);
            db.tx(t => {
                const qlist = [];
                for(var i = 0;i<queryArray.length;i++){
                    qlist.push(t.none(queryArray[i]));
                }
                return t.batch(qlist); // all of the queries are to be resolved;
            })
            .then(data => {
                resolve(true);
            })
            .catch(error => {
                reject(error);
            });
        }catch(error){
            reject(error);
        }
    });
}