'use strict';
const routerGift = require('express').Router();
var cors = require('cors');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
//const elasticSearch = require('./elasticSearch/es_logic');
//const elasticSearch2 = require('./elasticSearch/es_logic2');
//const elasticSearchGetData = require('./elasticSearch/es_logicGetData');
const masterData = require('./masterData/masterDataHelper');
const giftHelper = require('./gift/giftHandleDataUpload');
const giftHelperFoodEx = require('./gift/giftHandleFoodExUpload');
const giftFirebaseHelper = require('./gift/giftFirebaseHandler');
const giftIndicatorsHelper = require('./gift/getIndicatorsData');
const giftHelperCodeBook = require('./gift/giftHandleCodeBookUpload');

var corsOptions = {
  origin: 'http://example.com',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

routerGift.route('*', cors());

routerGift.route('/').get((req, res) => {
    res.send('Hello from App Engine for GIFT project!');
});

routerGift.route('/giftGetIndicatorsData').post(cors(corsOptions),function (req, res) {
    return giftIndicatorsHelper.handleGetIndicatorDataRequest(req, res);
}
);


routerGift.route('/giftGetMetadataResource').post(function (req, res) {
    return giftFirebaseHelper.giftGetMetadataResource(req,res);
}
);

routerGift.route('/giftGetFirebaseData').post(function (req, res) {    
 return giftFirebaseHelper.giftGetFirebaseData(req,res);
}
);

routerGift.route('/getMasterData').post(function (req, res) {
    return masterData.handleRequest(req,res);
});

routerGift.route('giftGetDisclaimer').post(function (req, res) {
    giftFirebaseHelper.giftGetDisclaimer(req,res);
});

routerGift.route('giftGetAcknowledgmentData').post(function (req, res) {
    giftFirebaseHelper.giftGetAcknowledgmentData(req,res);
});

routerGift.route('giftGetFilteredMetadata').post(function (req, res) {
    giftIndicatorsHelper.handleGetFilteredMetadata(req,res);
});

routerGift.route('giftUploadCodeBook').post(function (req, res) {
    giftHelperCodeBook.handleUploadCodeBookRequest(req,res);    
});

routerGift.route('giftUploadFoodEx').post(function (req, res) {
    giftHelperFoodEx.handleUploadFoodExRequest(req,res);    
});

routerGift.route('giftUploadData').post(function (req, res) {
    giftHelper.handleUploadDataRequest(req,res);    
});

routerGift.route('giftGetData').post(function (req, res) {
    giftHelper.handleGetDataRequest(req,res);    
});

routerGift.route('giftListFiles').post(function (req, res) {
    giftHelper.handleListFilesrequest(req,res);
});

routerGift.route('giftDownloadMicrodata').post(function (req, res) {
    giftHelper.handleDownloadrequest(req,res);
});

routerGift.get('/about', function (req, res) {
    console.log(req.body);
    res.send('About this wiki');
});


module.exports = routerGift;